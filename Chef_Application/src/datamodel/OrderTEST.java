/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import java.time.LocalTime;
import java.util.ArrayList;

/**
 * @author otjornelund
 */
public class OrderTEST 
{
    //Class attributes
    private int orderID;
    private LocalTime orderTime;
    private String orderStatus; //CHANGE LATER
    private ArrayList<OrderItemTEST> orderItemList;
    
    
    //Constructor for TEST purposes
    public OrderTEST(int orderID, LocalTime orderTime, String orderStatus, ArrayList<OrderItemTEST> orderItemList) {
        this.orderID = orderID;
        this.orderTime = orderTime;
        this.orderStatus = orderStatus;
        this.orderItemList = orderItemList;
    }
    
    
    //Read-Only Property Accessors
    public int getOrderID() {
        return orderID;
    }

    public LocalTime getOrderTime() {
        return orderTime;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public ArrayList<OrderItemTEST> getOrderItemList() {
        return orderItemList;
    }
    
    public Boolean isPrepared()
    {
        Boolean result = true;
        
        for (OrderItemTEST currentOrderItem : orderItemList)
        {
            if (!currentOrderItem.getIsPrepared())
                result = false;
        }
        
        return result;
    }
}
