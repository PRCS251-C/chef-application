/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import java.util.ArrayList;

/**
 * @author otjornelund
 */
public class OrderItemTEST 
{
    //Class attributes
    private Integer orderItemID;
    private String productName;
    private String orderItemSize;
    private ArrayList<String> customisationList;   //populated at runtime
    private Boolean isPrepared;

    
    //Constructor
    public OrderItemTEST(int orderItemID, String productName, String orderItemSize, ArrayList<String> customisationList) {
        this.orderItemID = orderItemID;
        this.productName = productName;
        this.orderItemSize = orderItemSize;
        this.customisationList = customisationList;
        this.isPrepared = false;
    }
    
    
    //Read-Only Property Accessors
    public Integer getOrderItemID() {
        return orderItemID;
    }

    public String getProductName() {
        return productName;
    }

    public String getOrderItemSize() {
        return orderItemSize;
    }

    public ArrayList<String> getCustomisationList() {
        return customisationList;
    }

    public Boolean getIsPrepared() {
        return isPrepared;
    }

    public void setIsPrepared(Boolean isPrepared) {
        this.isPrepared = isPrepared;
    }
    
}
