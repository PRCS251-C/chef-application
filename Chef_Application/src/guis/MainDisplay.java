/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



package guis;

import datamodel.*;
import java.awt.Color;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Queue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.LinkedList;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;



/**
 *
 * @author otjornelund
 */
public class MainDisplay extends javax.swing.JFrame
{
    // - - - - - - - - - - - - GLOBAL VARIABLES - - - - - - - - - - - - - - - -
    private Color customLightBlue = new Color(110, 200, 255);
    private Color customOrange = new Color(255, 185, 50);
    
    ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);    //Used as the "timer" that triggers the displays to update
    int fifteenSecondCounter = 0;
    int sixtySecondCounter = 0;
    private Runnable updateDisplaysRunnable = new Runnable()                    //The timer "ticks" every second: code for this is in MainDisplay()
        {  
            public void run()                                                   //Code that runs at the behest of the "timer"
            {
                updatePanelTimeDisplays(); 
                fifteenSecondCounter++;
                sixtySecondCounter++;
                
                if (fifteenSecondCounter == 15)
                {
                    updateOrderDisplays();
                    updateLocalDateTimeDisplay();
                    fifteenSecondCounter = 0;
                }
                
                if (sixtySecondCounter == 60)
                {
                    updateOrderData();
                    sixtySecondCounter = 0;
                }
            }
        }; 
    
    private Order[] panelArray = new Order[12];
    private LinkedList<Order> orderBacklogQueue = new LinkedList();
    private ArrayList<Order> ordersAwaitingDeliveryList = new ArrayList<>();
    
    private DefaultListModel ordersAwaitingDeliveryListModel = new DefaultListModel();
    private DefaultListModel orderBacklogListModel = new DefaultListModel();
    
    
    
    /**
     * Creates new form MainDisplay
     */
    public MainDisplay() 
    {
        refreshOrdersAwaitingDeliveryListModel();
        refreshOrderBacklogListModel();
        
        //Set program-wide "timer" to "tick" every second
        executor.scheduleAtFixedRate(updateDisplaysRunnable, 0, 1, TimeUnit.SECONDS);  //"ticks" every 1 second
        
        initComponents();
        
        OrderPanel1.setBackground(Color.lightGray);
        OrderPanel2.setBackground(Color.lightGray);
        OrderPanel3.setBackground(Color.lightGray);
        OrderPanel4.setBackground(Color.lightGray);
        OrderPanel5.setBackground(Color.lightGray);
        OrderPanel6.setBackground(Color.lightGray);
        OrderPanel7.setBackground(Color.lightGray);
        OrderPanel8.setBackground(Color.lightGray);
        OrderPanel9.setBackground(Color.lightGray);
        OrderPanel10.setBackground(Color.lightGray);
        OrderPanel11.setBackground(Color.lightGray);
        OrderPanel12.setBackground(Color.lightGray);
        
        updateOrderData();
        updateLocalDateTimeDisplay();
    } // End of MainDisplay constructor
    

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        OrderPanel1 = new javax.swing.JPanel();
        lblIdLabelPanel1 = new javax.swing.JLabel();
        lblOrderIdPanel1 = new javax.swing.JLabel();
        lblTimePanel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtOrderPanel1 = new javax.swing.JTextArea();
        lblStateLabelPanel1 = new javax.swing.JLabel();
        lblStatePanel1 = new javax.swing.JLabel();
        btnChangeStatePanel1 = new javax.swing.JButton();
        OrderPanel2 = new javax.swing.JPanel();
        lblIdLabelPanel2 = new javax.swing.JLabel();
        lblOrderIdPanel2 = new javax.swing.JLabel();
        lblTimePanel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtOrderPanel2 = new javax.swing.JTextArea();
        lblStateLabelPanel2 = new javax.swing.JLabel();
        lblStatePanel2 = new javax.swing.JLabel();
        btnChangeStatePanel2 = new javax.swing.JButton();
        OrderPanel3 = new javax.swing.JPanel();
        lblIdLabelPanel3 = new javax.swing.JLabel();
        lblOrderIdPanel3 = new javax.swing.JLabel();
        lblTimePanel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtOrderPanel3 = new javax.swing.JTextArea();
        lblStateLabelPanel3 = new javax.swing.JLabel();
        lblStatePanel3 = new javax.swing.JLabel();
        btnChangeStatePanel3 = new javax.swing.JButton();
        OrderPanel4 = new javax.swing.JPanel();
        lblIdLabelPanel4 = new javax.swing.JLabel();
        lblOrderIdPanel4 = new javax.swing.JLabel();
        lblTimePanel4 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtOrderPanel4 = new javax.swing.JTextArea();
        lblStateLabelPanel4 = new javax.swing.JLabel();
        lblStatePanel4 = new javax.swing.JLabel();
        btnChangeStatePanel4 = new javax.swing.JButton();
        OrderPanel5 = new javax.swing.JPanel();
        lblIdLabelPanel5 = new javax.swing.JLabel();
        lblOrderIdPanel5 = new javax.swing.JLabel();
        lblTimePanel5 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtOrderPanel5 = new javax.swing.JTextArea();
        lblStateLabelPanel5 = new javax.swing.JLabel();
        lblStatePanel5 = new javax.swing.JLabel();
        btnChangeStatePanel5 = new javax.swing.JButton();
        OrderPanel6 = new javax.swing.JPanel();
        lblIdLabelPanel6 = new javax.swing.JLabel();
        lblOrderIdPanel6 = new javax.swing.JLabel();
        lblTimePanel6 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        txtOrderPanel6 = new javax.swing.JTextArea();
        lblStateLabelPanel6 = new javax.swing.JLabel();
        lblStatePanel6 = new javax.swing.JLabel();
        btnChangeStatePanel6 = new javax.swing.JButton();
        OrderPanel7 = new javax.swing.JPanel();
        lblIdLabelPanel7 = new javax.swing.JLabel();
        lblOrderIdPanel7 = new javax.swing.JLabel();
        lblTimePanel7 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        txtOrderPanel7 = new javax.swing.JTextArea();
        lblStateLabelPanel7 = new javax.swing.JLabel();
        lblStatePanel7 = new javax.swing.JLabel();
        btnChangeStatePanel7 = new javax.swing.JButton();
        OrderPanel8 = new javax.swing.JPanel();
        lblIdLabelPanel8 = new javax.swing.JLabel();
        lblOrderIdPanel8 = new javax.swing.JLabel();
        lblTimePanel8 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        txtOrderPanel8 = new javax.swing.JTextArea();
        lblStateLabelPanel8 = new javax.swing.JLabel();
        lblStatePanel8 = new javax.swing.JLabel();
        btnChangeStatePanel8 = new javax.swing.JButton();
        OrderPanel9 = new javax.swing.JPanel();
        lblIdLabelPanel9 = new javax.swing.JLabel();
        lblOrderIdPanel9 = new javax.swing.JLabel();
        lblTimePanel9 = new javax.swing.JLabel();
        jScrollPane11 = new javax.swing.JScrollPane();
        txtOrderPanel9 = new javax.swing.JTextArea();
        lblStateLabelPanel9 = new javax.swing.JLabel();
        lblStatePanel9 = new javax.swing.JLabel();
        btnChangeStatePanel9 = new javax.swing.JButton();
        OrderPanel10 = new javax.swing.JPanel();
        lblIdLabelPanel10 = new javax.swing.JLabel();
        lblOrderIdPanel10 = new javax.swing.JLabel();
        lblTimePanel10 = new javax.swing.JLabel();
        jScrollPane12 = new javax.swing.JScrollPane();
        txtOrderPanel10 = new javax.swing.JTextArea();
        lblStateLabelPanel10 = new javax.swing.JLabel();
        lblStatePanel10 = new javax.swing.JLabel();
        btnChangeStatePanel10 = new javax.swing.JButton();
        OrderPanel11 = new javax.swing.JPanel();
        lblIdLabelPanel11 = new javax.swing.JLabel();
        lblOrderIdPanel11 = new javax.swing.JLabel();
        lblTimePanel11 = new javax.swing.JLabel();
        jScrollPane13 = new javax.swing.JScrollPane();
        txtOrderPanel11 = new javax.swing.JTextArea();
        lblStateLabelPanel11 = new javax.swing.JLabel();
        lblStatePanel11 = new javax.swing.JLabel();
        btnChangeStatePanel11 = new javax.swing.JButton();
        OrderPanel12 = new javax.swing.JPanel();
        lblIdLabelPanel12 = new javax.swing.JLabel();
        lblOrderIdPanel12 = new javax.swing.JLabel();
        lblTimePanel12 = new javax.swing.JLabel();
        jScrollPane14 = new javax.swing.JScrollPane();
        txtOrderPanel12 = new javax.swing.JTextArea();
        lblStateLabelPanel12 = new javax.swing.JLabel();
        lblStatePanel12 = new javax.swing.JLabel();
        btnChangeStatePanel12 = new javax.swing.JButton();
        OrderBacklogPanel = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        lstOrderBacklog = new javax.swing.JList();
        OrdersAwaitingDeliveryPanel = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane10 = new javax.swing.JScrollPane();
        lstOrdersAwaitingDelivery = new javax.swing.JList();
        jPanel1 = new javax.swing.JPanel();
        lblCurrentDateTime = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(153, 153, 153));

        OrderPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51), 2));
        OrderPanel1.setPreferredSize(new java.awt.Dimension(177, 188));

        lblIdLabelPanel1.setText("ID:");
        lblIdLabelPanel1.setEnabled(false);

        lblOrderIdPanel1.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblOrderIdPanel1.setText("---");
        lblOrderIdPanel1.setEnabled(false);

        lblTimePanel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTimePanel1.setText("---");
        lblTimePanel1.setEnabled(false);

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtOrderPanel1.setEditable(false);
        txtOrderPanel1.setBackground(new java.awt.Color(51, 51, 51));
        txtOrderPanel1.setColumns(20);
        txtOrderPanel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtOrderPanel1.setForeground(new java.awt.Color(255, 255, 0));
        txtOrderPanel1.setRows(5);
        txtOrderPanel1.setEnabled(false);
        jScrollPane1.setViewportView(txtOrderPanel1);

        lblStateLabelPanel1.setText("Status:");
        lblStateLabelPanel1.setEnabled(false);

        lblStatePanel1.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblStatePanel1.setText("---");
        lblStatePanel1.setEnabled(false);

        btnChangeStatePanel1.setText("NEXT");
        btnChangeStatePanel1.setEnabled(false);
        btnChangeStatePanel1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeStatePanel1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout OrderPanel1Layout = new javax.swing.GroupLayout(OrderPanel1);
        OrderPanel1.setLayout(OrderPanel1Layout);
        OrderPanel1Layout.setHorizontalGroup(
            OrderPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(OrderPanel1Layout.createSequentialGroup()
                        .addComponent(lblIdLabelPanel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblOrderIdPanel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTimePanel1))
                    .addGroup(OrderPanel1Layout.createSequentialGroup()
                        .addComponent(lblStateLabelPanel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblStatePanel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnChangeStatePanel1)))
                .addContainerGap())
        );
        OrderPanel1Layout.setVerticalGroup(
            OrderPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIdLabelPanel1)
                    .addComponent(lblOrderIdPanel1)
                    .addComponent(lblTimePanel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(OrderPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStateLabelPanel1)
                    .addComponent(btnChangeStatePanel1)
                    .addComponent(lblStatePanel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addContainerGap())
        );

        OrderPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51), 2));

        lblIdLabelPanel2.setText("ID:");
        lblIdLabelPanel2.setEnabled(false);

        lblOrderIdPanel2.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblOrderIdPanel2.setText("---");
        lblOrderIdPanel2.setEnabled(false);

        lblTimePanel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTimePanel2.setText("---");
        lblTimePanel2.setEnabled(false);

        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtOrderPanel2.setEditable(false);
        txtOrderPanel2.setBackground(new java.awt.Color(51, 51, 51));
        txtOrderPanel2.setColumns(20);
        txtOrderPanel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtOrderPanel2.setForeground(new java.awt.Color(255, 255, 0));
        txtOrderPanel2.setRows(5);
        txtOrderPanel2.setEnabled(false);
        jScrollPane2.setViewportView(txtOrderPanel2);

        lblStateLabelPanel2.setText("Status:");
        lblStateLabelPanel2.setEnabled(false);

        lblStatePanel2.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblStatePanel2.setText("---");
        lblStatePanel2.setEnabled(false);

        btnChangeStatePanel2.setText("NEXT");
        btnChangeStatePanel2.setEnabled(false);
        btnChangeStatePanel2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeStatePanel2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout OrderPanel2Layout = new javax.swing.GroupLayout(OrderPanel2);
        OrderPanel2.setLayout(OrderPanel2Layout);
        OrderPanel2Layout.setHorizontalGroup(
            OrderPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(OrderPanel2Layout.createSequentialGroup()
                        .addComponent(lblIdLabelPanel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblOrderIdPanel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTimePanel2))
                    .addGroup(OrderPanel2Layout.createSequentialGroup()
                        .addComponent(lblStateLabelPanel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblStatePanel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnChangeStatePanel2)))
                .addContainerGap())
        );
        OrderPanel2Layout.setVerticalGroup(
            OrderPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIdLabelPanel2)
                    .addComponent(lblOrderIdPanel2)
                    .addComponent(lblTimePanel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(OrderPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStateLabelPanel2)
                    .addComponent(btnChangeStatePanel2)
                    .addComponent(lblStatePanel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2)
                .addContainerGap())
        );

        OrderPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51), 2));

        lblIdLabelPanel3.setText("ID:");
        lblIdLabelPanel3.setEnabled(false);

        lblOrderIdPanel3.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblOrderIdPanel3.setText("---");
        lblOrderIdPanel3.setEnabled(false);

        lblTimePanel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTimePanel3.setText("---");
        lblTimePanel3.setEnabled(false);

        jScrollPane3.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtOrderPanel3.setEditable(false);
        txtOrderPanel3.setBackground(new java.awt.Color(51, 51, 51));
        txtOrderPanel3.setColumns(20);
        txtOrderPanel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtOrderPanel3.setForeground(new java.awt.Color(255, 255, 0));
        txtOrderPanel3.setRows(5);
        txtOrderPanel3.setEnabled(false);
        jScrollPane3.setViewportView(txtOrderPanel3);

        lblStateLabelPanel3.setText("Status:");
        lblStateLabelPanel3.setEnabled(false);

        lblStatePanel3.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblStatePanel3.setText("---");
        lblStatePanel3.setEnabled(false);

        btnChangeStatePanel3.setText("NEXT");
        btnChangeStatePanel3.setEnabled(false);
        btnChangeStatePanel3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeStatePanel3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout OrderPanel3Layout = new javax.swing.GroupLayout(OrderPanel3);
        OrderPanel3.setLayout(OrderPanel3Layout);
        OrderPanel3Layout.setHorizontalGroup(
            OrderPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(OrderPanel3Layout.createSequentialGroup()
                        .addComponent(lblIdLabelPanel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblOrderIdPanel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTimePanel3))
                    .addGroup(OrderPanel3Layout.createSequentialGroup()
                        .addComponent(lblStateLabelPanel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblStatePanel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnChangeStatePanel3)))
                .addContainerGap())
        );
        OrderPanel3Layout.setVerticalGroup(
            OrderPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIdLabelPanel3)
                    .addComponent(lblOrderIdPanel3)
                    .addComponent(lblTimePanel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(OrderPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStateLabelPanel3)
                    .addComponent(btnChangeStatePanel3)
                    .addComponent(lblStatePanel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3)
                .addContainerGap())
        );

        OrderPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51), 2));

        lblIdLabelPanel4.setText("ID:");
        lblIdLabelPanel4.setEnabled(false);

        lblOrderIdPanel4.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblOrderIdPanel4.setText("---");
        lblOrderIdPanel4.setEnabled(false);

        lblTimePanel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTimePanel4.setText("---");
        lblTimePanel4.setEnabled(false);

        jScrollPane4.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtOrderPanel4.setEditable(false);
        txtOrderPanel4.setBackground(new java.awt.Color(51, 51, 51));
        txtOrderPanel4.setColumns(20);
        txtOrderPanel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtOrderPanel4.setForeground(new java.awt.Color(255, 255, 0));
        txtOrderPanel4.setRows(5);
        txtOrderPanel4.setEnabled(false);
        jScrollPane4.setViewportView(txtOrderPanel4);

        lblStateLabelPanel4.setText("Status:");
        lblStateLabelPanel4.setEnabled(false);

        lblStatePanel4.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblStatePanel4.setText("---");
        lblStatePanel4.setEnabled(false);

        btnChangeStatePanel4.setText("NEXT");
        btnChangeStatePanel4.setEnabled(false);
        btnChangeStatePanel4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeStatePanel4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout OrderPanel4Layout = new javax.swing.GroupLayout(OrderPanel4);
        OrderPanel4.setLayout(OrderPanel4Layout);
        OrderPanel4Layout.setHorizontalGroup(
            OrderPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(OrderPanel4Layout.createSequentialGroup()
                        .addComponent(lblIdLabelPanel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblOrderIdPanel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTimePanel4))
                    .addGroup(OrderPanel4Layout.createSequentialGroup()
                        .addComponent(lblStateLabelPanel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblStatePanel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnChangeStatePanel4)))
                .addContainerGap())
        );
        OrderPanel4Layout.setVerticalGroup(
            OrderPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIdLabelPanel4)
                    .addComponent(lblOrderIdPanel4)
                    .addComponent(lblTimePanel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(OrderPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStateLabelPanel4)
                    .addComponent(btnChangeStatePanel4)
                    .addComponent(lblStatePanel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4)
                .addContainerGap())
        );

        OrderPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51), 2));

        lblIdLabelPanel5.setText("ID:");
        lblIdLabelPanel5.setEnabled(false);

        lblOrderIdPanel5.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblOrderIdPanel5.setText("---");
        lblOrderIdPanel5.setEnabled(false);

        lblTimePanel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTimePanel5.setText("---");
        lblTimePanel5.setEnabled(false);

        jScrollPane5.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtOrderPanel5.setEditable(false);
        txtOrderPanel5.setBackground(new java.awt.Color(51, 51, 51));
        txtOrderPanel5.setColumns(20);
        txtOrderPanel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtOrderPanel5.setForeground(new java.awt.Color(255, 255, 0));
        txtOrderPanel5.setRows(5);
        txtOrderPanel5.setEnabled(false);
        jScrollPane5.setViewportView(txtOrderPanel5);

        lblStateLabelPanel5.setText("Status:");
        lblStateLabelPanel5.setEnabled(false);

        lblStatePanel5.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblStatePanel5.setText("---");
        lblStatePanel5.setEnabled(false);

        btnChangeStatePanel5.setText("NEXT");
        btnChangeStatePanel5.setEnabled(false);
        btnChangeStatePanel5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeStatePanel5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout OrderPanel5Layout = new javax.swing.GroupLayout(OrderPanel5);
        OrderPanel5.setLayout(OrderPanel5Layout);
        OrderPanel5Layout.setHorizontalGroup(
            OrderPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(OrderPanel5Layout.createSequentialGroup()
                        .addComponent(lblIdLabelPanel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblOrderIdPanel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTimePanel5))
                    .addGroup(OrderPanel5Layout.createSequentialGroup()
                        .addComponent(lblStateLabelPanel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblStatePanel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnChangeStatePanel5)))
                .addContainerGap())
        );
        OrderPanel5Layout.setVerticalGroup(
            OrderPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIdLabelPanel5)
                    .addComponent(lblOrderIdPanel5)
                    .addComponent(lblTimePanel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(OrderPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStateLabelPanel5)
                    .addComponent(btnChangeStatePanel5)
                    .addComponent(lblStatePanel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5)
                .addContainerGap())
        );

        OrderPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51), 2));

        lblIdLabelPanel6.setText("ID:");
        lblIdLabelPanel6.setEnabled(false);

        lblOrderIdPanel6.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblOrderIdPanel6.setText("---");
        lblOrderIdPanel6.setEnabled(false);

        lblTimePanel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTimePanel6.setText("---");
        lblTimePanel6.setEnabled(false);

        jScrollPane6.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtOrderPanel6.setEditable(false);
        txtOrderPanel6.setBackground(new java.awt.Color(51, 51, 51));
        txtOrderPanel6.setColumns(20);
        txtOrderPanel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtOrderPanel6.setForeground(new java.awt.Color(255, 255, 0));
        txtOrderPanel6.setRows(5);
        txtOrderPanel6.setEnabled(false);
        jScrollPane6.setViewportView(txtOrderPanel6);

        lblStateLabelPanel6.setText("Status:");
        lblStateLabelPanel6.setEnabled(false);

        lblStatePanel6.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblStatePanel6.setText("---");
        lblStatePanel6.setEnabled(false);

        btnChangeStatePanel6.setText("NEXT");
        btnChangeStatePanel6.setEnabled(false);
        btnChangeStatePanel6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeStatePanel6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout OrderPanel6Layout = new javax.swing.GroupLayout(OrderPanel6);
        OrderPanel6.setLayout(OrderPanel6Layout);
        OrderPanel6Layout.setHorizontalGroup(
            OrderPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(OrderPanel6Layout.createSequentialGroup()
                        .addComponent(lblIdLabelPanel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblOrderIdPanel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTimePanel6))
                    .addGroup(OrderPanel6Layout.createSequentialGroup()
                        .addComponent(lblStateLabelPanel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblStatePanel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnChangeStatePanel6)))
                .addContainerGap())
        );
        OrderPanel6Layout.setVerticalGroup(
            OrderPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIdLabelPanel6)
                    .addComponent(lblOrderIdPanel6)
                    .addComponent(lblTimePanel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(OrderPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStateLabelPanel6)
                    .addComponent(btnChangeStatePanel6)
                    .addComponent(lblStatePanel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6)
                .addContainerGap())
        );

        OrderPanel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51), 2));
        OrderPanel7.setPreferredSize(new java.awt.Dimension(177, 178));

        lblIdLabelPanel7.setText("ID:");
        lblIdLabelPanel7.setEnabled(false);

        lblOrderIdPanel7.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblOrderIdPanel7.setText("---");
        lblOrderIdPanel7.setEnabled(false);

        lblTimePanel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTimePanel7.setText("---");
        lblTimePanel7.setEnabled(false);

        jScrollPane7.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtOrderPanel7.setEditable(false);
        txtOrderPanel7.setBackground(new java.awt.Color(51, 51, 51));
        txtOrderPanel7.setColumns(20);
        txtOrderPanel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtOrderPanel7.setForeground(new java.awt.Color(255, 255, 0));
        txtOrderPanel7.setRows(5);
        txtOrderPanel7.setEnabled(false);
        jScrollPane7.setViewportView(txtOrderPanel7);

        lblStateLabelPanel7.setText("Status:");
        lblStateLabelPanel7.setEnabled(false);

        lblStatePanel7.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblStatePanel7.setText("---");
        lblStatePanel7.setEnabled(false);

        btnChangeStatePanel7.setText("NEXT");
        btnChangeStatePanel7.setEnabled(false);
        btnChangeStatePanel7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeStatePanel7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout OrderPanel7Layout = new javax.swing.GroupLayout(OrderPanel7);
        OrderPanel7.setLayout(OrderPanel7Layout);
        OrderPanel7Layout.setHorizontalGroup(
            OrderPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(OrderPanel7Layout.createSequentialGroup()
                        .addComponent(lblIdLabelPanel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblOrderIdPanel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTimePanel7))
                    .addGroup(OrderPanel7Layout.createSequentialGroup()
                        .addComponent(lblStateLabelPanel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblStatePanel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnChangeStatePanel7)))
                .addContainerGap())
        );
        OrderPanel7Layout.setVerticalGroup(
            OrderPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIdLabelPanel7)
                    .addComponent(lblOrderIdPanel7)
                    .addComponent(lblTimePanel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(OrderPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStateLabelPanel7)
                    .addComponent(btnChangeStatePanel7)
                    .addComponent(lblStatePanel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7)
                .addContainerGap())
        );

        OrderPanel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51), 2));

        lblIdLabelPanel8.setText("ID:");
        lblIdLabelPanel8.setEnabled(false);

        lblOrderIdPanel8.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblOrderIdPanel8.setText("---");
        lblOrderIdPanel8.setEnabled(false);

        lblTimePanel8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTimePanel8.setText("---");
        lblTimePanel8.setEnabled(false);

        jScrollPane8.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtOrderPanel8.setEditable(false);
        txtOrderPanel8.setBackground(new java.awt.Color(51, 51, 51));
        txtOrderPanel8.setColumns(20);
        txtOrderPanel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtOrderPanel8.setForeground(new java.awt.Color(255, 255, 0));
        txtOrderPanel8.setRows(5);
        txtOrderPanel8.setEnabled(false);
        jScrollPane8.setViewportView(txtOrderPanel8);

        lblStateLabelPanel8.setText("Status:");
        lblStateLabelPanel8.setEnabled(false);

        lblStatePanel8.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblStatePanel8.setText("---");
        lblStatePanel8.setEnabled(false);

        btnChangeStatePanel8.setText("NEXT");
        btnChangeStatePanel8.setEnabled(false);
        btnChangeStatePanel8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeStatePanel8ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout OrderPanel8Layout = new javax.swing.GroupLayout(OrderPanel8);
        OrderPanel8.setLayout(OrderPanel8Layout);
        OrderPanel8Layout.setHorizontalGroup(
            OrderPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(OrderPanel8Layout.createSequentialGroup()
                        .addComponent(lblIdLabelPanel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblOrderIdPanel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTimePanel8))
                    .addGroup(OrderPanel8Layout.createSequentialGroup()
                        .addComponent(lblStateLabelPanel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblStatePanel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnChangeStatePanel8)))
                .addContainerGap())
        );
        OrderPanel8Layout.setVerticalGroup(
            OrderPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIdLabelPanel8)
                    .addComponent(lblOrderIdPanel8)
                    .addComponent(lblTimePanel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(OrderPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStateLabelPanel8)
                    .addComponent(btnChangeStatePanel8)
                    .addComponent(lblStatePanel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane8)
                .addContainerGap())
        );

        OrderPanel9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51), 2));

        lblIdLabelPanel9.setText("ID:");
        lblIdLabelPanel9.setEnabled(false);

        lblOrderIdPanel9.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblOrderIdPanel9.setText("---");
        lblOrderIdPanel9.setEnabled(false);

        lblTimePanel9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTimePanel9.setText("---");
        lblTimePanel9.setEnabled(false);

        jScrollPane11.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtOrderPanel9.setEditable(false);
        txtOrderPanel9.setBackground(new java.awt.Color(51, 51, 51));
        txtOrderPanel9.setColumns(20);
        txtOrderPanel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtOrderPanel9.setForeground(new java.awt.Color(255, 255, 0));
        txtOrderPanel9.setRows(5);
        txtOrderPanel9.setEnabled(false);
        jScrollPane11.setViewportView(txtOrderPanel9);

        lblStateLabelPanel9.setText("Status:");
        lblStateLabelPanel9.setEnabled(false);

        lblStatePanel9.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblStatePanel9.setText("---");
        lblStatePanel9.setEnabled(false);

        btnChangeStatePanel9.setText("NEXT");
        btnChangeStatePanel9.setEnabled(false);
        btnChangeStatePanel9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeStatePanel9ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout OrderPanel9Layout = new javax.swing.GroupLayout(OrderPanel9);
        OrderPanel9.setLayout(OrderPanel9Layout);
        OrderPanel9Layout.setHorizontalGroup(
            OrderPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(OrderPanel9Layout.createSequentialGroup()
                        .addComponent(lblIdLabelPanel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblOrderIdPanel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTimePanel9))
                    .addGroup(OrderPanel9Layout.createSequentialGroup()
                        .addComponent(lblStateLabelPanel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblStatePanel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnChangeStatePanel9)))
                .addContainerGap())
        );
        OrderPanel9Layout.setVerticalGroup(
            OrderPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIdLabelPanel9)
                    .addComponent(lblOrderIdPanel9)
                    .addComponent(lblTimePanel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(OrderPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStateLabelPanel9)
                    .addComponent(btnChangeStatePanel9)
                    .addComponent(lblStatePanel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane11)
                .addContainerGap())
        );

        OrderPanel10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51), 2));

        lblIdLabelPanel10.setText("ID:");
        lblIdLabelPanel10.setEnabled(false);

        lblOrderIdPanel10.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblOrderIdPanel10.setText("---");
        lblOrderIdPanel10.setEnabled(false);

        lblTimePanel10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTimePanel10.setText("---");
        lblTimePanel10.setEnabled(false);

        jScrollPane12.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtOrderPanel10.setEditable(false);
        txtOrderPanel10.setBackground(new java.awt.Color(51, 51, 51));
        txtOrderPanel10.setColumns(20);
        txtOrderPanel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtOrderPanel10.setForeground(new java.awt.Color(255, 255, 0));
        txtOrderPanel10.setRows(5);
        txtOrderPanel10.setEnabled(false);
        jScrollPane12.setViewportView(txtOrderPanel10);

        lblStateLabelPanel10.setText("Status:");
        lblStateLabelPanel10.setEnabled(false);

        lblStatePanel10.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblStatePanel10.setText("---");
        lblStatePanel10.setEnabled(false);

        btnChangeStatePanel10.setText("NEXT");
        btnChangeStatePanel10.setEnabled(false);
        btnChangeStatePanel10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeStatePanel10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout OrderPanel10Layout = new javax.swing.GroupLayout(OrderPanel10);
        OrderPanel10.setLayout(OrderPanel10Layout);
        OrderPanel10Layout.setHorizontalGroup(
            OrderPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(OrderPanel10Layout.createSequentialGroup()
                        .addComponent(lblIdLabelPanel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblOrderIdPanel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTimePanel10))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, OrderPanel10Layout.createSequentialGroup()
                        .addComponent(lblStateLabelPanel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblStatePanel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnChangeStatePanel10)))
                .addContainerGap())
        );
        OrderPanel10Layout.setVerticalGroup(
            OrderPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblOrderIdPanel10)
                    .addComponent(lblIdLabelPanel10)
                    .addComponent(lblTimePanel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(OrderPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnChangeStatePanel10)
                    .addComponent(lblStateLabelPanel10)
                    .addComponent(lblStatePanel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane12)
                .addContainerGap())
        );

        OrderPanel11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51), 2));

        lblIdLabelPanel11.setText("ID:");
        lblIdLabelPanel11.setEnabled(false);

        lblOrderIdPanel11.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblOrderIdPanel11.setText("---");
        lblOrderIdPanel11.setEnabled(false);

        lblTimePanel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTimePanel11.setText("---");
        lblTimePanel11.setEnabled(false);

        jScrollPane13.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtOrderPanel11.setEditable(false);
        txtOrderPanel11.setBackground(new java.awt.Color(51, 51, 51));
        txtOrderPanel11.setColumns(20);
        txtOrderPanel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtOrderPanel11.setForeground(new java.awt.Color(255, 255, 0));
        txtOrderPanel11.setRows(5);
        txtOrderPanel11.setEnabled(false);
        jScrollPane13.setViewportView(txtOrderPanel11);

        lblStateLabelPanel11.setText("Status:");
        lblStateLabelPanel11.setEnabled(false);

        lblStatePanel11.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblStatePanel11.setText("---");
        lblStatePanel11.setEnabled(false);

        btnChangeStatePanel11.setText("NEXT");
        btnChangeStatePanel11.setEnabled(false);
        btnChangeStatePanel11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeStatePanel11ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout OrderPanel11Layout = new javax.swing.GroupLayout(OrderPanel11);
        OrderPanel11.setLayout(OrderPanel11Layout);
        OrderPanel11Layout.setHorizontalGroup(
            OrderPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(OrderPanel11Layout.createSequentialGroup()
                        .addComponent(lblIdLabelPanel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblOrderIdPanel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTimePanel11))
                    .addGroup(OrderPanel11Layout.createSequentialGroup()
                        .addComponent(lblStateLabelPanel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblStatePanel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnChangeStatePanel11)))
                .addContainerGap())
        );
        OrderPanel11Layout.setVerticalGroup(
            OrderPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIdLabelPanel11)
                    .addComponent(lblOrderIdPanel11)
                    .addComponent(lblTimePanel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(OrderPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStateLabelPanel11)
                    .addComponent(btnChangeStatePanel11)
                    .addComponent(lblStatePanel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane13)
                .addContainerGap())
        );

        OrderPanel12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51), 2));

        lblIdLabelPanel12.setText("ID:");
        lblIdLabelPanel12.setEnabled(false);

        lblOrderIdPanel12.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblOrderIdPanel12.setText("---");
        lblOrderIdPanel12.setEnabled(false);

        lblTimePanel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblTimePanel12.setText("---");
        lblTimePanel12.setEnabled(false);

        jScrollPane14.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtOrderPanel12.setEditable(false);
        txtOrderPanel12.setBackground(new java.awt.Color(51, 51, 51));
        txtOrderPanel12.setColumns(20);
        txtOrderPanel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtOrderPanel12.setForeground(new java.awt.Color(255, 255, 0));
        txtOrderPanel12.setRows(5);
        txtOrderPanel12.setEnabled(false);
        jScrollPane14.setViewportView(txtOrderPanel12);

        lblStateLabelPanel12.setText("Status:");
        lblStateLabelPanel12.setEnabled(false);

        lblStatePanel12.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblStatePanel12.setText("---");
        lblStatePanel12.setEnabled(false);

        btnChangeStatePanel12.setText("NEXT");
        btnChangeStatePanel12.setEnabled(false);
        btnChangeStatePanel12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeStatePanel12ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout OrderPanel12Layout = new javax.swing.GroupLayout(OrderPanel12);
        OrderPanel12.setLayout(OrderPanel12Layout);
        OrderPanel12Layout.setHorizontalGroup(
            OrderPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(OrderPanel12Layout.createSequentialGroup()
                        .addComponent(lblIdLabelPanel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblOrderIdPanel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTimePanel12))
                    .addGroup(OrderPanel12Layout.createSequentialGroup()
                        .addComponent(lblStateLabelPanel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblStatePanel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnChangeStatePanel12)))
                .addContainerGap())
        );
        OrderPanel12Layout.setVerticalGroup(
            OrderPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIdLabelPanel12)
                    .addComponent(lblOrderIdPanel12)
                    .addComponent(lblTimePanel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(OrderPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStateLabelPanel12)
                    .addComponent(btnChangeStatePanel12)
                    .addComponent(lblStatePanel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane14)
                .addContainerGap())
        );

        OrderBacklogPanel.setBackground(new java.awt.Color(255, 153, 204));
        OrderBacklogPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51), 2));

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel18.setText("Order Backlog");

        jScrollPane9.setBackground(new java.awt.Color(255, 204, 204));

        lstOrderBacklog.setBackground(new java.awt.Color(51, 51, 51));
        lstOrderBacklog.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lstOrderBacklog.setForeground(new java.awt.Color(0, 255, 255));
        lstOrderBacklog.setModel(orderBacklogListModel);
        lstOrderBacklog.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstOrderBacklogValueChanged(evt);
            }
        });
        jScrollPane9.setViewportView(lstOrderBacklog);

        javax.swing.GroupLayout OrderBacklogPanelLayout = new javax.swing.GroupLayout(OrderBacklogPanel);
        OrderBacklogPanel.setLayout(OrderBacklogPanelLayout);
        OrderBacklogPanelLayout.setHorizontalGroup(
            OrderBacklogPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, OrderBacklogPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrderBacklogPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        OrderBacklogPanelLayout.setVerticalGroup(
            OrderBacklogPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrderBacklogPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE)
                .addContainerGap())
        );

        OrdersAwaitingDeliveryPanel.setBackground(new java.awt.Color(153, 255, 255));
        OrdersAwaitingDeliveryPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51), 2));

        jLabel8.setBackground(new java.awt.Color(153, 204, 255));
        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Orders Awaiting Delivery");

        jScrollPane10.setBackground(new java.awt.Color(204, 255, 204));

        lstOrdersAwaitingDelivery.setBackground(new java.awt.Color(51, 51, 51));
        lstOrdersAwaitingDelivery.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lstOrdersAwaitingDelivery.setForeground(new java.awt.Color(0, 255, 255));
        lstOrdersAwaitingDelivery.setModel(ordersAwaitingDeliveryListModel);
        lstOrdersAwaitingDelivery.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstOrdersAwaitingDeliveryValueChanged(evt);
            }
        });
        jScrollPane10.setViewportView(lstOrdersAwaitingDelivery);

        javax.swing.GroupLayout OrdersAwaitingDeliveryPanelLayout = new javax.swing.GroupLayout(OrdersAwaitingDeliveryPanel);
        OrdersAwaitingDeliveryPanel.setLayout(OrdersAwaitingDeliveryPanelLayout);
        OrdersAwaitingDeliveryPanelLayout.setHorizontalGroup(
            OrdersAwaitingDeliveryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, OrdersAwaitingDeliveryPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(OrdersAwaitingDeliveryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE))
                .addContainerGap())
        );
        OrdersAwaitingDeliveryPanelLayout.setVerticalGroup(
            OrdersAwaitingDeliveryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(OrdersAwaitingDeliveryPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblCurrentDateTime.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblCurrentDateTime.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCurrentDateTime.setText("current time");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(306, 306, 306)
                .addComponent(lblCurrentDateTime, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE)
                .addGap(306, 306, 306))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblCurrentDateTime)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(OrderPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE)
                            .addComponent(OrderPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(OrderPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(OrderPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(OrderPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(OrderPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(OrderPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(OrderPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(OrderPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(OrderPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(OrderPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(OrderPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(OrderBacklogPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(OrdersAwaitingDeliveryPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(OrderPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE)
                    .addComponent(OrderPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(OrderPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(OrderPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(OrderPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(OrderPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(OrderBacklogPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(OrderPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
                    .addComponent(OrderPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(OrderPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(OrderPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(OrderPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(OrderPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(OrdersAwaitingDeliveryPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    
    // - - - - - - - - EVENT HANDLERS FOR STATE CHANGE BUTTONS - - - - - - - - -
    
    private void btnChangeStatePanel1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeStatePanel1ActionPerformed
        switch (lblStatePanel1.getText())
        {
            case "Received":
                setOrderState(panelArray[0], "Preparing");
                displayOrderState(1);
                break;
            case "Preparing":
                setOrderState(panelArray[0], "Baking");
                displayOrderState(1);
                break;
            case "Baking":
                //Dialog to confirm with user whether order is ready to be sent to await delivery
                AwaitingDeliveryConfirmationDialog dialog = new AwaitingDeliveryConfirmationDialog(null, true);
                dialog.setParent(this);
                dialog.setParams(panelArray[0], 1);
                dialog.setVisible(true);
                break;
            default:
                System.out.println("ERROR: Invalid Order State selection was made!");
                break;
        }
    }//GEN-LAST:event_btnChangeStatePanel1ActionPerformed

    private void btnChangeStatePanel2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeStatePanel2ActionPerformed
        switch (lblStatePanel2.getText())
        {
            case "Received":
                setOrderState(panelArray[1], "Preparing");
                displayOrderState(2);
                break;
            case "Preparing":
                setOrderState(panelArray[1], "Baking");
                displayOrderState(2);
                break;
            case "Baking":
                //Dialog to confirm with user whether order is ready to be sent to await delivery
                AwaitingDeliveryConfirmationDialog dialog = new AwaitingDeliveryConfirmationDialog(null, true);
                dialog.setParent(this);
                dialog.setParams(panelArray[1], 2);
                dialog.setVisible(true);
                break;
            default:
                System.out.println("ERROR: Invalid Order State selection was made!");
                break;
        }
    }//GEN-LAST:event_btnChangeStatePanel2ActionPerformed

    private void btnChangeStatePanel3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeStatePanel3ActionPerformed
        switch (lblStatePanel3.getText())
        {
            case "Received":
                setOrderState(panelArray[2], "Preparing");
                displayOrderState(3);
                break;
            case "Preparing":
                setOrderState(panelArray[2], "Baking");
                displayOrderState(3);
                break;
            case "Baking":
                //Dialog to confirm with user whether order is ready to be sent to await delivery
                AwaitingDeliveryConfirmationDialog dialog = new AwaitingDeliveryConfirmationDialog(null, true);
                dialog.setParent(this);
                dialog.setParams(panelArray[2], 3);
                dialog.setVisible(true);
                break;
            default:
                System.out.println("ERROR: Invalid Order State selection was made!");
                break;
        }
    }//GEN-LAST:event_btnChangeStatePanel3ActionPerformed

    private void btnChangeStatePanel4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeStatePanel4ActionPerformed
        switch (lblStatePanel4.getText())
        {
            case "Received":
                setOrderState(panelArray[3], "Preparing");
                displayOrderState(4);
                break;
            case "Preparing":
                setOrderState(panelArray[3], "Baking");
                displayOrderState(4);
                break;
            case "Baking":
                //Dialog to confirm with user whether order is ready to be sent to await delivery
                AwaitingDeliveryConfirmationDialog dialog = new AwaitingDeliveryConfirmationDialog(null, true);
                dialog.setParent(this);
                dialog.setParams(panelArray[3], 4);
                dialog.setVisible(true);
                break;
            default:
                System.out.println("ERROR: Invalid Order State selection was made!");
                break;
        }
    }//GEN-LAST:event_btnChangeStatePanel4ActionPerformed

    private void btnChangeStatePanel5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeStatePanel5ActionPerformed
        switch (lblStatePanel5.getText())
        {
            case "Received":
                setOrderState(panelArray[4], "Preparing");
                displayOrderState(5);
                break;
            case "Preparing":
                setOrderState(panelArray[4], "Baking");
                displayOrderState(5);
                break;
            case "Baking":
                //Dialog to confirm with user whether order is ready to be sent to await delivery
                AwaitingDeliveryConfirmationDialog dialog = new AwaitingDeliveryConfirmationDialog(null, true);
                dialog.setParent(this);
                dialog.setParams(panelArray[4], 5);
                dialog.setVisible(true);
                break;
            default:
                System.out.println("ERROR: Invalid Order State selection was made!");
                break;
        }
    }//GEN-LAST:event_btnChangeStatePanel5ActionPerformed

    private void btnChangeStatePanel6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeStatePanel6ActionPerformed
        switch (lblStatePanel6.getText())
        {
            case "Received":
                setOrderState(panelArray[5], "Preparing");
                displayOrderState(6);
                break;
            case "Preparing":
                setOrderState(panelArray[5], "Baking");
                displayOrderState(6);
                break;
            case "Baking":
                //Dialog to confirm with user whether order is ready to be sent to await delivery
                AwaitingDeliveryConfirmationDialog dialog = new AwaitingDeliveryConfirmationDialog(null, true);
                dialog.setParent(this);
                dialog.setParams(panelArray[5], 6);
                dialog.setVisible(true);
                break;
            default:
                System.out.println("ERROR: Invalid Order State selection was made!");
                break;
        }
    }//GEN-LAST:event_btnChangeStatePanel6ActionPerformed

    private void btnChangeStatePanel7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeStatePanel7ActionPerformed
        switch (lblStatePanel7.getText())
        {
            case "Received":
                setOrderState(panelArray[6], "Preparing");
                displayOrderState(7);
                break;
            case "Preparing":
                setOrderState(panelArray[6], "Baking");
                displayOrderState(7);
                break;
            case "Baking":
                //Dialog to confirm with user whether order is ready to be sent to await delivery
                AwaitingDeliveryConfirmationDialog dialog = new AwaitingDeliveryConfirmationDialog(null, true);
                dialog.setParent(this);
                dialog.setParams(panelArray[6], 7);
                dialog.setVisible(true);
                break;
            default:
                System.out.println("ERROR: Invalid Order State selection was made!");
                break;
        }
    }//GEN-LAST:event_btnChangeStatePanel7ActionPerformed

    private void btnChangeStatePanel8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeStatePanel8ActionPerformed
        switch (lblStatePanel8.getText())
        {
            case "Received":
                setOrderState(panelArray[7], "Preparing");
                displayOrderState(8);
                break;
            case "Preparing":
                setOrderState(panelArray[7], "Baking");
                displayOrderState(8);
                break;
            case "Baking":
                //Dialog to confirm with user whether order is ready to be sent to await delivery
                AwaitingDeliveryConfirmationDialog dialog = new AwaitingDeliveryConfirmationDialog(null, true);
                dialog.setParent(this);
                dialog.setParams(panelArray[7], 8);
                dialog.setVisible(true);
                break;
            default:
                System.out.println("ERROR: Invalid Order State selection was made!");
                break;
        }
    }//GEN-LAST:event_btnChangeStatePanel8ActionPerformed

    private void btnChangeStatePanel9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeStatePanel9ActionPerformed
        switch (lblStatePanel9.getText())
        {
            case "Received":
                setOrderState(panelArray[8], "Preparing");
                displayOrderState(9);
                break;
            case "Preparing":
                setOrderState(panelArray[8], "Baking");
                displayOrderState(9);
                break;
            case "Baking":
                //Dialog to confirm with user whether order is ready to be sent to await delivery
                AwaitingDeliveryConfirmationDialog dialog = new AwaitingDeliveryConfirmationDialog(null, true);
                dialog.setParent(this);
                dialog.setParams(panelArray[8], 9);
                dialog.setVisible(true);
                break;
            default:
                System.out.println("ERROR: Invalid Order State selection was made!");
                break;
        }
    }//GEN-LAST:event_btnChangeStatePanel9ActionPerformed

    private void btnChangeStatePanel10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeStatePanel10ActionPerformed
        switch (lblStatePanel10.getText())
        {
            case "Received":
                setOrderState(panelArray[9], "Preparing");
                displayOrderState(10);
                break;
            case "Preparing":
                setOrderState(panelArray[9], "Baking");
                displayOrderState(10);
                break;
            case "Baking":
                //Dialog to confirm with user whether order is ready to be sent to await delivery
                AwaitingDeliveryConfirmationDialog dialog = new AwaitingDeliveryConfirmationDialog(null, true);
                dialog.setParent(this);
                dialog.setParams(panelArray[9], 10);
                dialog.setVisible(true);
                break;
            default:
                System.out.println("ERROR: Invalid Order State selection was made!");
                break;
        }
    }//GEN-LAST:event_btnChangeStatePanel10ActionPerformed

    private void btnChangeStatePanel11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeStatePanel11ActionPerformed
        switch (lblStatePanel11.getText())
        {
            case "Received":
                setOrderState(panelArray[10], "Preparing");
                displayOrderState(11);
                break;
            case "Preparing":
                setOrderState(panelArray[10], "Baking");
                displayOrderState(11);
                break;
            case "Baking":
                //Dialog to confirm with user whether order is ready to be sent to await delivery
                AwaitingDeliveryConfirmationDialog dialog = new AwaitingDeliveryConfirmationDialog(null, true);
                dialog.setParent(this);
                dialog.setParams(panelArray[10], 11);
                dialog.setVisible(true);
                break;
            default:
                System.out.println("ERROR: Invalid Order State selection was made!");
                break;
        }
    }//GEN-LAST:event_btnChangeStatePanel11ActionPerformed

    private void btnChangeStatePanel12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeStatePanel12ActionPerformed
        switch (lblStatePanel12.getText())
        {
            case "Received":
                setOrderState(panelArray[11], "Preparing");
                displayOrderState(12);
                break;
            case "Preparing":
                setOrderState(panelArray[11], "Baking");
                displayOrderState(12);
                break;
            case "Baking":
                //Dialog to confirm with user whether order is ready to be sent to await delivery
                AwaitingDeliveryConfirmationDialog dialog = new AwaitingDeliveryConfirmationDialog(null, true);
                dialog.setParent(this);
                dialog.setParams(panelArray[11], 12);
                dialog.setVisible(true);
                break;
            default:
                System.out.println("ERROR: Invalid Order State selection was made!");
                break;
        }
    }//GEN-LAST:event_btnChangeStatePanel12ActionPerformed

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -    
    
    
    
    // - - - - - - - - - - - EVENT HANDLERS FOR LIST BOXES - - - - - - - - - - -
    
    private void lstOrderBacklogValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstOrderBacklogValueChanged
        if (lstOrderBacklog.getSelectedIndex() > -1)
        { 
            //Get the Order object selected in the Backlog list on the UI.
            Order selectedOrder = (Order)lstOrderBacklog.getSelectedValue();
            
            //Display a message box pop-up detailing the Items of that Order
            OrderInformationDialog dialog = new OrderInformationDialog(null, true);
                dialog.setParent(this);
                dialog.setOrder(selectedOrder);
                dialog.setVisible(true);

            //Refresh list model to clear the list of possible errors caused by the selection process.
            refreshOrderBacklogListModel();
        }
    }//GEN-LAST:event_lstOrderBacklogValueChanged

    private void lstOrdersAwaitingDeliveryValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstOrdersAwaitingDeliveryValueChanged
        if (lstOrdersAwaitingDelivery.getSelectedIndex() > -1)
        {
            //Get the Order object selected in the Backlog list on the UI.
            Order selectedOrder = (Order)lstOrdersAwaitingDelivery.getSelectedValue();

            //Display a message box pop-up detailing the Items of that Order
            OrderInformationDialog dialog = new OrderInformationDialog(null, true);
                dialog.setParent(this);
                dialog.setOrder(selectedOrder);
                dialog.setVisible(true);
            
            //Refresh list model to clear the list of possible errors caused by the selection process.
            refreshOrdersAwaitingDeliveryListModel();
        }
    }//GEN-LAST:event_lstOrdersAwaitingDeliveryValueChanged
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -   
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) 
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainDisplay.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainDisplay.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainDisplay.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainDisplay.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                MainDisplay display = new MainDisplay();
                display.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                display.setExtendedState(JFrame.MAXIMIZED_BOTH);
                display.setVisible(true);
            }
        });
    } // End of Main method
    
    
    
    /**
     * Synchronises the Orders in this program's data model with those of the back-end database.
     * Fetches newly-placed Orders and adds them to this program's Backlog.
     * @param orderList 
     */
    private void updateOrderData()
    {
        //Refresh contents of local data model's Order list.
        ModelController.getInstance().populateOrderData();
        
        //This boolean flags whether the program is already handling the Order.
        Boolean contained = false;
        
        //These lists temporaily store the orders and aid with adding them to the backlog in priority order.
        ArrayList<Order> orderPlacedList = new ArrayList<>();
        ArrayList<Order> orderReceivedList = new ArrayList<>();
        ArrayList<Order> preparingList = new ArrayList<>();
        ArrayList<Order> bakingList = new ArrayList<>();
                        
        //For every order in the local data model's Order list...
        for (Order currentOrder : ModelController.getInstance().getOrderList())
        {   
            if ( currentOrder.getStatus().equals("Awaiting Delivery") )
            {
                //If the order's status is "Awaiting Delivery": Add it straight to this program's Awaiting Delivery list!
                
                //Assess whetehr the Order is already present in this program's Awaiting Delivery List...
                for (Order order : ordersAwaitingDeliveryList) 
                {
                    if (order.getOrderId().equals(currentOrder.getOrderId()))
                        contained = true;
                }   
                
                if (!contained)
                    ordersAwaitingDeliveryList.add(currentOrder);
            }
            else if ( currentOrder.getStatus().equals("Out For Delivery") ) 
            {
                //If an Order becomes "Out For Delivery": Find it in the Awaiting-Delivery list and remove it.
                
                for (Order order : ordersAwaitingDeliveryList) 
                {
                    if (order.getOrderId().equals(currentOrder.getOrderId()))
                    {
                        //ordersAwaitingDeliveryList.remove(order);
                        contained = true;
                    }
                }
                
                if (contained) 
                {
                    for (int i = 0; i < ordersAwaitingDeliveryList.size(); i++) 
                    {
                        if (ordersAwaitingDeliveryList.get(i).getOrderId().equals(currentOrder.getOrderId())) 
                        {
                            ordersAwaitingDeliveryList.remove(i);
                        }
                    }
                }
            }
            else
            {               
                //Assess whether the Order is already present in this program's Order Panels...
                for (int i = 0; i < panelArray.length; i++) 
                {
                    if (panelArray[i] != null) {
                        if (panelArray[i].getOrderId().equals(currentOrder.getOrderId()))
                            contained = true;
                    }
                } 
                
                //Assess whether the the Order is already present in this program's Backlog...
                for (Order order : orderBacklogQueue) 
                {
                    if (order.getOrderId().equals(currentOrder.getOrderId()))
                        contained = true;
                }
                
                //If the order is a "new" order not currently being handled by the program...
                if (!contained)
                {
                    String orderState = currentOrder.getStatus();
                    
                    //File the order into a temporary storage list based on its current state.
                    switch (orderState)
                    {
                        case "Order Placed":
                            orderPlacedList.add(currentOrder);
                            break;
                        case "Order Received":
                            orderReceivedList.add(currentOrder);
                            break;
                        case "Preparing":
                            preparingList.add(currentOrder);
                            break;
                        case "Baking":
                            bakingList.add(currentOrder);
                            break;
                        default:
                            System.out.println("ERROR: Attempted to handle Order with invalid state!");
                            break;
                    }
                }
            }
        }   // End of For-loop
        
        //Move the orders from the storage lists to the backlog, prioritising them based on their current state.
        if (!bakingList.isEmpty())
        {
            for (Order order : bakingList)
                receiveOrder(order);
        }
        if (!preparingList.isEmpty())
        {
            for (Order order : preparingList)
                receiveOrder(order);
        }
        if (!orderReceivedList.isEmpty())
        {
            for (Order order : orderReceivedList)
                receiveOrder(order);
        }
        if (!orderPlacedList.isEmpty())
        {
            for (Order order : orderPlacedList)
            {
                //If the Order's status is "Order Placed", it means this program hasn't handled it yet.
                //Send the order through to this program's Backlog Queue, and set it's status to Received.
                setOrderState(order, "Order Received");
                receiveOrder(order);
            }
        }
        
        refreshOrdersAwaitingDeliveryListModel();
    }
    
    
    
    /**
     * Changes an order's "State" attribute, both in the local data model and also on the back end database.
     * @param order
     * @param state 
     */
    private void setOrderState(Order order, String state)
    {
        order.setStatus(state);
        ModelController.getInstance().updateOrder(order);
    }
    
    
    
    /**
     * Updates this program's Order Backlog with a newly-received Order.
     * NOTE: This method could be used later on to directly respond to a new Order being pushed by the system.
     * @param newOrder 
     */
    private void receiveOrder(Order newOrder)
    {
        orderBacklogQueue.add(newOrder);
        refreshOrderBacklogListModel();
    }
    
    
    
    /**
     * This method is responsible for pulling backlogged orders to the empty Order Panels
     * It also updates displays of "time elapsed" for the Order Backlog and Awaiting Delivery section.
     * It gets called every time a program-wide "timer" ticks a certain number of seconds.
     */
    private void updateOrderDisplays()
    {
        //If backlog is not empty... (this check could be removed, but it saves processing whenever the backlog is empty) 
        if (!orderBacklogQueue.isEmpty())
        {
            //Scan order panels for empty space
            for (int i = 0; i < panelArray.length; i++)
            {
                //If there's a space (and backlog still isn't empty): POLL next Order from backlog and call DisplayOrder
                if ( (panelArray[i] == null) && (!orderBacklogQueue.isEmpty()) )
                    displayOrder(orderBacklogQueue.poll(), i+1);
            }
        }

        //Update order backlog and awaiting-delivery-list displays
        //(This updates their "time elapsed" displays.)
        refreshOrderBacklogListModel();
        refreshOrdersAwaitingDeliveryListModel();
    }
    
    
    
    /**
     * Updates the "time elapsed" displays of all active Order Panels
     */
    private void updatePanelTimeDisplays()
    {
        //System.out.println("DEBUG: updatePanelTimeDisplays has run.");
        if (panelArray[0] != null)
            lblTimePanel1.setText(displayMinutesSecondsAgo(panelArray[0].getOrderTime()));
        
        if (panelArray[1] != null)
            lblTimePanel2.setText(displayMinutesSecondsAgo(panelArray[1].getOrderTime()));
        
        if (panelArray[2] != null)
            lblTimePanel3.setText(displayMinutesSecondsAgo(panelArray[2].getOrderTime()));
        
        if (panelArray[3] != null)
            lblTimePanel4.setText(displayMinutesSecondsAgo(panelArray[3].getOrderTime()));
        
        if (panelArray[4] != null)
            lblTimePanel5.setText(displayMinutesSecondsAgo(panelArray[4].getOrderTime()));
        
        if (panelArray[5] != null)
            lblTimePanel6.setText(displayMinutesSecondsAgo(panelArray[5].getOrderTime()));
        
        if (panelArray[6] != null)
            lblTimePanel7.setText(displayMinutesSecondsAgo(panelArray[6].getOrderTime()));
        
        if (panelArray[7] != null)
            lblTimePanel8.setText(displayMinutesSecondsAgo(panelArray[7].getOrderTime()));
        
        if (panelArray[8] != null)
            lblTimePanel9.setText(displayMinutesSecondsAgo(panelArray[8].getOrderTime()));
        
        if (panelArray[9] != null)
            lblTimePanel10.setText(displayMinutesSecondsAgo(panelArray[9].getOrderTime()));
        
        if (panelArray[10] != null)
            lblTimePanel11.setText(displayMinutesSecondsAgo(panelArray[10].getOrderTime()));
        
        if (panelArray[11] != null)
            lblTimePanel12.setText(displayMinutesSecondsAgo(panelArray[11].getOrderTime()));
    }
     
    
    
    private void updateLocalDateTimeDisplay()
    {
        StringBuilder dateTimeString = new StringBuilder(255);
        String ordinal;
        
        //Get the correct two-letter suffix to the current day of the month.
        switch (LocalDateTime.now().getDayOfMonth())
        {
            case 1:
                ordinal = "st";
                break;
            case 2:
                ordinal = "nd";
                break;
            case 3:
                ordinal = "rd";
                break;
            case 21:
                ordinal = "st";
                break;
            case 22:
                ordinal = "nd";
                break;
            case 23:
                ordinal = "rd";
                break;
            case 31:
                ordinal = "st";
                break;
            default:
                ordinal = "th";
                break;
        }
        
        //Build a pleasing string to describe the current date and time.
        dateTimeString.append(LocalDateTime.now().getDayOfWeek() + " ");
        dateTimeString.append(LocalDateTime.now().getDayOfMonth() + ordinal + " ");
        dateTimeString.append(LocalDateTime.now().getMonth() + " - ");
        dateTimeString.append(LocalDateTime.now().getHour() + ":" + LocalDateTime.now().getMinute());
        
        //Update the program's date-and-time display
        lblCurrentDateTime.setText("" + dateTimeString);
    }
    
    
    
    /**
     * Produces a String describing the time elapsed between now and the given
     * LocalDateTime, written in the form "Xh Ym ago".
     * @param time
     * @return 
     */
    private String displayHoursMinutesAgo(LocalDateTime time)
    {
        long totalMinutes = ChronoUnit.MINUTES.between(time, LocalDateTime.now());
        long hours = totalMinutes / 60;
        long minutes = totalMinutes % 60;
        
        if (hours > 0)
            return "" + hours + "h " + minutes + "m ago";
        else
            return "" + minutes + "m ago";
    }
    
    
    
    /**
     * Produces a String describing the time elapsed between now and the given
     * LocalDateTime, written in the form "Xm Ys ago".
     * @param time
     * @return 
     */
    private String displayMinutesSecondsAgo(LocalDateTime time)
    {  
        long totalSeconds = ChronoUnit.SECONDS.between(time, LocalDateTime.now());
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        
        if (minutes > 0)
            return "" + minutes + "m " + seconds + "s ago";
        else
            return "" + seconds + "s ago";
    }
    
    
    /**
     * Method which updates the Order Backlog display
     */
    private void refreshOrderBacklogListModel() 
    {
        orderBacklogListModel.clear();
        
        String orderString;
        
        if (!orderBacklogQueue.isEmpty())
        {
            for (Order currentOrder : orderBacklogQueue)
            {
                orderString = "ID: " + currentOrder.getOrderId() + " - " + 
                              currentOrder.getOrderItems().size() + " items - " +
                              displayHoursMinutesAgo(currentOrder.getOrderTime());
                currentOrder.setDisplayString(orderString);
                orderBacklogListModel.addElement(currentOrder);
            }
        }
    }
    
    
    
    /**
     * Method which updates the "Orders Awaiting Delivery" display
     */
    private void refreshOrdersAwaitingDeliveryListModel() 
    {
        ordersAwaitingDeliveryListModel.clear();
        
        String orderString;
        
        if (!ordersAwaitingDeliveryList.isEmpty())
        {
            for (Order currentOrder : ordersAwaitingDeliveryList)
            {
                orderString = "ID: " + currentOrder.getOrderId() + " - " + 
                              currentOrder.getOrderItems().size() + " items - " +
                              displayHoursMinutesAgo(currentOrder.getOrderTime());
                currentOrder.setDisplayString(orderString);         
                ordersAwaitingDeliveryListModel.addElement(currentOrder);
            }
        }
    }
    
    
    
    public String getOrderText(Order order)
    {
        StringBuilder orderText = new StringBuilder(1000);
        
        //For each order item in the new order...
        for (OrderItem currentOrderItem : order.getOrderItems())
        {
            //Add main order item information to order display text
            if (
                    (currentOrderItem.getOrderItemSize() != null) 
                    || (currentOrderItem.getProduct().getProductType().equals("Side"))
                    || (currentOrderItem.getProduct().getProductType().equals("Dessert"))
                )
                orderText.append(currentOrderItem.getOrderItemSize() + " ");
            
            orderText.append(currentOrderItem.getProduct().getProductName()); 
            
            if (currentOrderItem.getOrderItemQuantity() != null)
                orderText.append(" x" + currentOrderItem.getOrderItemQuantity());
            
            orderText.append("\n");
            
            //If order item has customisations: Display them.
            if (!currentOrderItem.getCustomisations().isEmpty())
            {
                //Traverse customisation list and print them as indented list below the order item
                for (String currentCustomisation : currentOrderItem.getCustomisations())
                {
                    orderText.append("    - " + currentCustomisation + "\n");
                }
            }
        }
        
        return orderText.toString();
    }
    
    
    
    /**
     * Method that displays a given Order's information on a given Order Panel
     * @param orderToDisplay
     * @param panelNumber 
     */
    private void displayOrder(Order orderToDisplay, Integer panelNumber)
    {
        //Register the Order to the correct index of panelArray.
        panelArray[panelNumber - 1] = orderToDisplay;
        
        //Begin creating the Order text that will be displayed in the Text Area
        String orderText = getOrderText(orderToDisplay);
        
        displayOrderState(panelNumber);
        
        //Switch statement to identify the correct Order Panel and fill its Displays with information
        switch (panelNumber)
        {
            case 1:
                lblIdLabelPanel1.setEnabled(true);
                lblOrderIdPanel1.setEnabled(true);
                lblOrderIdPanel1.setText("" + orderToDisplay.getOrderId());
                lblStateLabelPanel1.setEnabled(true);
                lblStatePanel1.setEnabled(true);
                btnChangeStatePanel1.setEnabled(true);
                lblTimePanel1.setEnabled(true);
                lblTimePanel1.setText(displayMinutesSecondsAgo(panelArray[0].getOrderTime()));
                txtOrderPanel1.setEnabled(true);
                txtOrderPanel1.setText("" + orderText);
                break;
            case 2:
                lblIdLabelPanel2.setEnabled(true);
                lblOrderIdPanel2.setEnabled(true);
                lblOrderIdPanel2.setText("" + orderToDisplay.getOrderId());
                lblStateLabelPanel2.setEnabled(true);
                lblStatePanel2.setEnabled(true);
                btnChangeStatePanel2.setEnabled(true);
                lblTimePanel2.setEnabled(true);
                lblTimePanel2.setText(displayMinutesSecondsAgo(panelArray[0].getOrderTime()));
                txtOrderPanel2.setEnabled(true);
                txtOrderPanel2.setText("" + orderText);
                break;
            case 3:
                lblIdLabelPanel3.setEnabled(true);
                lblOrderIdPanel3.setEnabled(true);
                lblOrderIdPanel3.setText("" + orderToDisplay.getOrderId());
                lblStateLabelPanel3.setEnabled(true);
                lblStatePanel3.setEnabled(true);
                btnChangeStatePanel3.setEnabled(true);
                lblTimePanel3.setEnabled(true);
                lblTimePanel3.setText(displayMinutesSecondsAgo(panelArray[0].getOrderTime()));
                txtOrderPanel3.setEnabled(true);
                txtOrderPanel3.setText("" + orderText);
                break;
            case 4:
                lblIdLabelPanel4.setEnabled(true);
                lblOrderIdPanel4.setEnabled(true);
                lblOrderIdPanel4.setText("" + orderToDisplay.getOrderId());
                lblStateLabelPanel4.setEnabled(true);
                lblStatePanel4.setEnabled(true);
                btnChangeStatePanel4.setEnabled(true);
                lblTimePanel4.setEnabled(true);
                lblTimePanel4.setText(displayMinutesSecondsAgo(panelArray[0].getOrderTime()));
                txtOrderPanel4.setEnabled(true);
                txtOrderPanel4.setText("" + orderText);
                break;
            case 5:
                lblIdLabelPanel5.setEnabled(true);
                lblOrderIdPanel5.setEnabled(true);
                lblOrderIdPanel5.setText("" + orderToDisplay.getOrderId());
                lblStateLabelPanel5.setEnabled(true);
                lblStatePanel5.setEnabled(true);
                btnChangeStatePanel5.setEnabled(true);
                lblTimePanel5.setEnabled(true);
                lblTimePanel5.setText(displayMinutesSecondsAgo(panelArray[0].getOrderTime()));
                txtOrderPanel5.setEnabled(true);
                txtOrderPanel5.setText("" + orderText);
                break;
            case 6:
                lblIdLabelPanel6.setEnabled(true);
                lblOrderIdPanel6.setEnabled(true);
                lblOrderIdPanel6.setText("" + orderToDisplay.getOrderId());
                lblStateLabelPanel6.setEnabled(true);
                lblStatePanel6.setEnabled(true);
                btnChangeStatePanel6.setEnabled(true);
                lblTimePanel6.setEnabled(true);
                lblTimePanel6.setText(displayMinutesSecondsAgo(panelArray[0].getOrderTime()));
                txtOrderPanel6.setEnabled(true);
                txtOrderPanel6.setText("" + orderText);
                break;
            case 7:
                lblIdLabelPanel7.setEnabled(true);
                lblOrderIdPanel7.setEnabled(true);
                lblOrderIdPanel7.setText("" + orderToDisplay.getOrderId());
                lblStateLabelPanel7.setEnabled(true);
                lblStatePanel7.setEnabled(true);
                btnChangeStatePanel7.setEnabled(true);
                lblTimePanel7.setEnabled(true);
                lblTimePanel7.setText(displayMinutesSecondsAgo(panelArray[0].getOrderTime()));
                txtOrderPanel7.setEnabled(true);
                txtOrderPanel7.setText("" + orderText);
                break;
            case 8:
                lblIdLabelPanel8.setEnabled(true);
                lblOrderIdPanel8.setEnabled(true);
                lblOrderIdPanel8.setText("" + orderToDisplay.getOrderId());
                lblStateLabelPanel8.setEnabled(true);
                lblStatePanel8.setEnabled(true);
                btnChangeStatePanel8.setEnabled(true);
                lblTimePanel8.setEnabled(true);
                lblTimePanel8.setText(displayMinutesSecondsAgo(panelArray[0].getOrderTime()));
                txtOrderPanel8.setEnabled(true);
                txtOrderPanel8.setText("" + orderText);
                break;
            case 9:
                lblIdLabelPanel9.setEnabled(true);
                lblOrderIdPanel9.setEnabled(true);
                lblOrderIdPanel9.setText("" + orderToDisplay.getOrderId());
                lblStateLabelPanel9.setEnabled(true);
                lblStatePanel9.setEnabled(true);
                btnChangeStatePanel9.setEnabled(true);
                lblTimePanel9.setEnabled(true);
                lblTimePanel9.setText(displayMinutesSecondsAgo(panelArray[0].getOrderTime()));
                txtOrderPanel9.setEnabled(true);
                txtOrderPanel9.setText("" + orderText);
                break;
            case 10:
                lblIdLabelPanel10.setEnabled(true);
                lblOrderIdPanel10.setEnabled(true);
                lblOrderIdPanel10.setText("" + orderToDisplay.getOrderId());
                lblStateLabelPanel10.setEnabled(true);
                lblStatePanel10.setEnabled(true);
                btnChangeStatePanel10.setEnabled(true);
                lblTimePanel10.setEnabled(true);
                lblTimePanel10.setText(displayMinutesSecondsAgo(panelArray[0].getOrderTime()));
                txtOrderPanel10.setEnabled(true);
                txtOrderPanel10.setText("" + orderText);
                break;
            case 11:
                lblIdLabelPanel11.setEnabled(true);
                lblOrderIdPanel11.setEnabled(true);
                lblOrderIdPanel11.setText("" + orderToDisplay.getOrderId());
                lblStateLabelPanel11.setEnabled(true);
                lblStatePanel11.setEnabled(true);
                btnChangeStatePanel11.setEnabled(true);
                lblTimePanel11.setEnabled(true);
                lblTimePanel11.setText(displayMinutesSecondsAgo(panelArray[0].getOrderTime()));
                txtOrderPanel11.setEnabled(true);
                txtOrderPanel11.setText("" + orderText);
                break;
            case 12:
                lblIdLabelPanel12.setEnabled(true);
                lblOrderIdPanel12.setEnabled(true);
                lblOrderIdPanel12.setText("" + orderToDisplay.getOrderId());
                lblStateLabelPanel12.setEnabled(true);
                lblStatePanel12.setEnabled(true);
                btnChangeStatePanel12.setEnabled(true);
                lblTimePanel12.setEnabled(true);
                lblTimePanel12.setText(displayMinutesSecondsAgo(panelArray[0].getOrderTime()));
                txtOrderPanel12.setEnabled(true);
                txtOrderPanel12.setText("" + orderText);
                break;
            default:
                System.out.println("ERROR: Panel number was invalid when attempting to display Order.");
                break;
        }
    }   // End of DisplayOrder method
    
    
    
    /**
     * Updates an Order Panel's display based on the status of the contained Order.
     * @param status
     * @param panelNumber 
     */
    private void displayOrderState(int panelNumber)
    {
        String stateText;
        Color stateColour;
        
        //If no order is being displayed in the panel, its "state" display features will be blanked out.
        if (panelArray[panelNumber-1] == null)
        {
            stateText = "---";
            stateColour = Color.lightGray;
        }
        else
        {
            //Else, the "state" display features will depend on the current state of the Order.
            switch (panelArray[panelNumber-1].getStatus())
            {
                case "Order Received":
                    stateText = "Received";
                    stateColour = Color.PINK;
                    break;
                case "Preparing":
                    stateText = "Preparing";
                    stateColour = customOrange;
                    break;
                case "Baking":
                    stateText = "Baking";
                    stateColour = Color.YELLOW;
                    break;
                default:
                    stateText = "ERROR";
                    stateColour = Color.MAGENTA;
                    break;
            }
        }
        
        //Actually set the "state" display features of the panel based on what was decided upon earlier.
        switch (panelNumber)
        {
            case 1:
                lblStatePanel1.setText(stateText);
                OrderPanel1.setBackground(stateColour);
                break;
            case 2:
                lblStatePanel2.setText(stateText);
                OrderPanel2.setBackground(stateColour);
                break;
            case 3:
                lblStatePanel3.setText(stateText);
                OrderPanel3.setBackground(stateColour);
                break;
            case 4:
                lblStatePanel4.setText(stateText);
                OrderPanel4.setBackground(stateColour);
                break;
            case 5:
                lblStatePanel5.setText(stateText);
                OrderPanel5.setBackground(stateColour);
                break;
            case 6:
                lblStatePanel6.setText(stateText);
                OrderPanel6.setBackground(stateColour);
                break;
            case 7:
                lblStatePanel7.setText(stateText);
                OrderPanel7.setBackground(stateColour);
                break;
            case 8:
                lblStatePanel8.setText(stateText);
                OrderPanel8.setBackground(stateColour);
                break;
            case 9:
                lblStatePanel9.setText(stateText);
                OrderPanel9.setBackground(stateColour);
                break;
            case 10:
                lblStatePanel10.setText(stateText);
                OrderPanel10.setBackground(stateColour);
                break;
            case 11:
                lblStatePanel11.setText(stateText);
                OrderPanel11.setBackground(stateColour);
                break;
            case 12:
                lblStatePanel12.setText(stateText);
                OrderPanel12.setBackground(stateColour);
                break;
        }
    }
    
    
    
    /**
     * Method that moves "Completed" order out of Order Panel display and into Orders Awaiting Delivery section.
     * @param orderToComplete
     * @param panelNumber 
     */
    public void completeOrder(Order orderToComplete, Integer panelNumber)
    {
        //Remove the completed order from the Array of orders being displayed in panels.
        panelArray[panelNumber-1] = null;
        
        //Set the order's state to "Awaiting Delivery".
        setOrderState(orderToComplete, "Awaiting Delivery");
        
        //Add the completed order to the Awaiting-Delivery list, then refresh said list on the UI.
        ordersAwaitingDeliveryList.add(orderToComplete);
        refreshOrdersAwaitingDeliveryListModel();
        
        //Reset the order "state" display features of the panel.
        displayOrderState(panelNumber);
        
        //Switch statement to identify the correct Order Panel and clear its Displays
        switch (panelNumber)
        {
            case 1:
                lblIdLabelPanel1.setEnabled(false);
                lblOrderIdPanel1.setEnabled(false);
                lblOrderIdPanel1.setText("---");
                lblStateLabelPanel1.setEnabled(false);
                lblStatePanel1.setEnabled(false);
                btnChangeStatePanel1.setEnabled(false);
                lblTimePanel1.setEnabled(false);
                lblTimePanel1.setText("---");
                txtOrderPanel1.setEnabled(false);
                txtOrderPanel1.setText("");
                break;
            case 2:
                lblIdLabelPanel2.setEnabled(false);
                lblOrderIdPanel2.setEnabled(false);
                lblOrderIdPanel2.setText("---");
                lblStateLabelPanel2.setEnabled(false);
                lblStatePanel2.setEnabled(false);
                btnChangeStatePanel2.setEnabled(false);
                lblTimePanel2.setEnabled(false);
                lblTimePanel2.setText("---");
                txtOrderPanel2.setEnabled(false);
                txtOrderPanel2.setText("");
                break;
            case 3:
                lblIdLabelPanel3.setEnabled(false);
                lblOrderIdPanel3.setEnabled(false);
                lblOrderIdPanel3.setText("---");
                lblStateLabelPanel3.setEnabled(false);
                lblStatePanel3.setEnabled(false);
                btnChangeStatePanel3.setEnabled(false);
                lblTimePanel3.setEnabled(false);
                lblTimePanel3.setText("---");
                txtOrderPanel3.setEnabled(false);
                txtOrderPanel3.setText("");
                break;
            case 4:
                lblIdLabelPanel4.setEnabled(false);
                lblOrderIdPanel4.setEnabled(false);
                lblOrderIdPanel4.setText("---");
                lblStateLabelPanel4.setEnabled(false);
                lblStatePanel4.setEnabled(false);
                btnChangeStatePanel4.setEnabled(false);
                lblTimePanel4.setEnabled(false);
                lblTimePanel4.setText("---");
                txtOrderPanel4.setEnabled(false);
                txtOrderPanel4.setText("");
                break;
            case 5:
                lblIdLabelPanel5.setEnabled(false);
                lblOrderIdPanel5.setEnabled(false);
                lblOrderIdPanel5.setText("---");
                lblStateLabelPanel5.setEnabled(false);
                lblStatePanel5.setEnabled(false);
                btnChangeStatePanel5.setEnabled(false);
                lblTimePanel5.setEnabled(false);
                lblTimePanel5.setText("---");
                txtOrderPanel5.setEnabled(false);
                txtOrderPanel5.setText("");
                break;
            case 6:
                lblIdLabelPanel6.setEnabled(false);
                lblOrderIdPanel6.setEnabled(false);
                lblOrderIdPanel6.setText("---");
                lblStateLabelPanel6.setEnabled(false);
                lblStatePanel6.setEnabled(false);
                btnChangeStatePanel6.setEnabled(false);
                lblTimePanel6.setEnabled(false);
                lblTimePanel6.setText("---");
                txtOrderPanel6.setEnabled(false);
                txtOrderPanel6.setText("");
                break;
            case 7:
                lblIdLabelPanel7.setEnabled(false);
                lblOrderIdPanel7.setEnabled(false);
                lblOrderIdPanel7.setText("---");
                lblStateLabelPanel7.setEnabled(false);
                lblStatePanel7.setEnabled(false);
                btnChangeStatePanel7.setEnabled(false);
                lblTimePanel7.setEnabled(false);
                lblTimePanel7.setText("---");
                txtOrderPanel7.setEnabled(false);
                txtOrderPanel7.setText("");
                break;
            case 8:
                lblIdLabelPanel8.setEnabled(false);
                lblOrderIdPanel8.setEnabled(false);
                lblOrderIdPanel8.setText("---");
                lblStateLabelPanel8.setEnabled(false);
                lblStatePanel8.setEnabled(false);
                btnChangeStatePanel8.setEnabled(false);
                lblTimePanel8.setEnabled(false);
                lblTimePanel8.setText("---");
                txtOrderPanel8.setEnabled(false);
                txtOrderPanel8.setText("");
                break;
            case 9:
                lblIdLabelPanel9.setEnabled(false);
                lblOrderIdPanel9.setEnabled(false);
                lblOrderIdPanel9.setText("---");
                lblStateLabelPanel9.setEnabled(false);
                lblStatePanel9.setEnabled(false);
                btnChangeStatePanel9.setEnabled(false);
                lblTimePanel9.setEnabled(false);
                lblTimePanel9.setText("---");
                txtOrderPanel9.setEnabled(false);
                txtOrderPanel9.setText("");
                break;
            case 10:
                lblIdLabelPanel10.setEnabled(false);
                lblOrderIdPanel10.setEnabled(false);
                lblOrderIdPanel10.setText("---");
                lblStateLabelPanel10.setEnabled(false);
                lblStatePanel10.setEnabled(false);
                btnChangeStatePanel10.setEnabled(false);
                lblTimePanel10.setEnabled(false);
                lblTimePanel10.setText("---");
                txtOrderPanel10.setEnabled(false);
                txtOrderPanel10.setText("");
                break;
            case 11:
                lblIdLabelPanel11.setEnabled(false);
                lblOrderIdPanel11.setEnabled(false);
                lblOrderIdPanel11.setText("---");
                lblStateLabelPanel11.setEnabled(false);
                lblStatePanel11.setEnabled(false);
                btnChangeStatePanel11.setEnabled(false);
                lblTimePanel11.setEnabled(false);
                lblTimePanel11.setText("---");
                txtOrderPanel11.setEnabled(false);
                txtOrderPanel11.setText("");
                break;
            case 12:
                lblIdLabelPanel12.setEnabled(false);
                lblOrderIdPanel12.setEnabled(false);
                lblOrderIdPanel12.setText("---");
                lblStateLabelPanel12.setEnabled(false);
                lblStatePanel12.setEnabled(false);
                btnChangeStatePanel12.setEnabled(false);
                lblTimePanel12.setEnabled(false);
                lblTimePanel12.setText("---");
                txtOrderPanel12.setEnabled(false);
                txtOrderPanel12.setText("");
                break;
            default:
                System.out.println("ERROR: Panel number was invalid when attempting to complete Order.");
                break;
        }
    }
    
    
    
    // ------------------------------------------------------------------------

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel OrderBacklogPanel;
    private javax.swing.JPanel OrderPanel1;
    private javax.swing.JPanel OrderPanel10;
    private javax.swing.JPanel OrderPanel11;
    private javax.swing.JPanel OrderPanel12;
    private javax.swing.JPanel OrderPanel2;
    private javax.swing.JPanel OrderPanel3;
    private javax.swing.JPanel OrderPanel4;
    private javax.swing.JPanel OrderPanel5;
    private javax.swing.JPanel OrderPanel6;
    private javax.swing.JPanel OrderPanel7;
    private javax.swing.JPanel OrderPanel8;
    private javax.swing.JPanel OrderPanel9;
    private javax.swing.JPanel OrdersAwaitingDeliveryPanel;
    private javax.swing.JButton btnChangeStatePanel1;
    private javax.swing.JButton btnChangeStatePanel10;
    private javax.swing.JButton btnChangeStatePanel11;
    private javax.swing.JButton btnChangeStatePanel12;
    private javax.swing.JButton btnChangeStatePanel2;
    private javax.swing.JButton btnChangeStatePanel3;
    private javax.swing.JButton btnChangeStatePanel4;
    private javax.swing.JButton btnChangeStatePanel5;
    private javax.swing.JButton btnChangeStatePanel6;
    private javax.swing.JButton btnChangeStatePanel7;
    private javax.swing.JButton btnChangeStatePanel8;
    private javax.swing.JButton btnChangeStatePanel9;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JLabel lblCurrentDateTime;
    private javax.swing.JLabel lblIdLabelPanel1;
    private javax.swing.JLabel lblIdLabelPanel10;
    private javax.swing.JLabel lblIdLabelPanel11;
    private javax.swing.JLabel lblIdLabelPanel12;
    private javax.swing.JLabel lblIdLabelPanel2;
    private javax.swing.JLabel lblIdLabelPanel3;
    private javax.swing.JLabel lblIdLabelPanel4;
    private javax.swing.JLabel lblIdLabelPanel5;
    private javax.swing.JLabel lblIdLabelPanel6;
    private javax.swing.JLabel lblIdLabelPanel7;
    private javax.swing.JLabel lblIdLabelPanel8;
    private javax.swing.JLabel lblIdLabelPanel9;
    private javax.swing.JLabel lblOrderIdPanel1;
    private javax.swing.JLabel lblOrderIdPanel10;
    private javax.swing.JLabel lblOrderIdPanel11;
    private javax.swing.JLabel lblOrderIdPanel12;
    private javax.swing.JLabel lblOrderIdPanel2;
    private javax.swing.JLabel lblOrderIdPanel3;
    private javax.swing.JLabel lblOrderIdPanel4;
    private javax.swing.JLabel lblOrderIdPanel5;
    private javax.swing.JLabel lblOrderIdPanel6;
    private javax.swing.JLabel lblOrderIdPanel7;
    private javax.swing.JLabel lblOrderIdPanel8;
    private javax.swing.JLabel lblOrderIdPanel9;
    private javax.swing.JLabel lblStateLabelPanel1;
    private javax.swing.JLabel lblStateLabelPanel10;
    private javax.swing.JLabel lblStateLabelPanel11;
    private javax.swing.JLabel lblStateLabelPanel12;
    private javax.swing.JLabel lblStateLabelPanel2;
    private javax.swing.JLabel lblStateLabelPanel3;
    private javax.swing.JLabel lblStateLabelPanel4;
    private javax.swing.JLabel lblStateLabelPanel5;
    private javax.swing.JLabel lblStateLabelPanel6;
    private javax.swing.JLabel lblStateLabelPanel7;
    private javax.swing.JLabel lblStateLabelPanel8;
    private javax.swing.JLabel lblStateLabelPanel9;
    private javax.swing.JLabel lblStatePanel1;
    private javax.swing.JLabel lblStatePanel10;
    private javax.swing.JLabel lblStatePanel11;
    private javax.swing.JLabel lblStatePanel12;
    private javax.swing.JLabel lblStatePanel2;
    private javax.swing.JLabel lblStatePanel3;
    private javax.swing.JLabel lblStatePanel4;
    private javax.swing.JLabel lblStatePanel5;
    private javax.swing.JLabel lblStatePanel6;
    private javax.swing.JLabel lblStatePanel7;
    private javax.swing.JLabel lblStatePanel8;
    private javax.swing.JLabel lblStatePanel9;
    private javax.swing.JLabel lblTimePanel1;
    private javax.swing.JLabel lblTimePanel10;
    private javax.swing.JLabel lblTimePanel11;
    private javax.swing.JLabel lblTimePanel12;
    private javax.swing.JLabel lblTimePanel2;
    private javax.swing.JLabel lblTimePanel3;
    private javax.swing.JLabel lblTimePanel4;
    private javax.swing.JLabel lblTimePanel5;
    private javax.swing.JLabel lblTimePanel6;
    private javax.swing.JLabel lblTimePanel7;
    private javax.swing.JLabel lblTimePanel8;
    private javax.swing.JLabel lblTimePanel9;
    private javax.swing.JList lstOrderBacklog;
    private javax.swing.JList lstOrdersAwaitingDelivery;
    private javax.swing.JTextArea txtOrderPanel1;
    private javax.swing.JTextArea txtOrderPanel10;
    private javax.swing.JTextArea txtOrderPanel11;
    private javax.swing.JTextArea txtOrderPanel12;
    private javax.swing.JTextArea txtOrderPanel2;
    private javax.swing.JTextArea txtOrderPanel3;
    private javax.swing.JTextArea txtOrderPanel4;
    private javax.swing.JTextArea txtOrderPanel5;
    private javax.swing.JTextArea txtOrderPanel6;
    private javax.swing.JTextArea txtOrderPanel7;
    private javax.swing.JTextArea txtOrderPanel8;
    private javax.swing.JTextArea txtOrderPanel9;
    // End of variables declaration//GEN-END:variables
   
} // End of MainDisplay Class
